let $startPrice := 1000
let $endPrice := 2000

for $book in doc("books")/all_books/book
    where $book/price >= $startPrice and $book/price <= $endPrice
    return concat(
            $book/@id, ', ',
            $book/author[position() = 1], ', ',
            $book/title, ', ',
            $book/city, ', ',
            $book/publisher, ', ',
            $book/year, ', ',
            $book/cover, ', ',
            $book/price, ', '
        )