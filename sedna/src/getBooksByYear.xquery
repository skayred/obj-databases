let $startYear := 2005
let $endYear := 2007

for $book in doc("books")/all_books/book
    where $book/year >= $startYear and $book/year <= $endYear
    return concat(
            $book/@id, ', ',
            $book/author[position() = 1], ', ',
            $book/title, ', ',
            $book/city, ', ',
            $book/publisher, ', ',
            $book/year, ', ',
            $book/cover, ', ',
            $book/price, ', '
        )