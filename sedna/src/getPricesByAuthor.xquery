declare function local:getBooksByAuthor ($author as xs:string) as item()* {
    for $book in doc("books")/all_books/book
        where $book/author = $author
        return $book/price
};

let $author := "Брукс Ф."

return sum(local:getBooksByAuthor($author))