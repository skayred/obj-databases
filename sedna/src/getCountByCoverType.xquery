declare function local:getBooksByCoverType ($cover as xs:string) as item()* {
    for $book in doc("books")/all_books/book
        where $book/cover = $cover
        return $book
};

concat(
    count(local:getBooksByCoverType("твердый")), "
",
    count(local:getBooksByCoverType("мягкий")), "
"
)