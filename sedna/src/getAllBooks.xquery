for $book in doc("books")/all_books/book
    return concat(
            $book/@id, ', ',
            $book/author[position() = 1], ', ',
            $book/title, ', ',
            $book/city, ', ',
            $book/publisher, ', ',
            $book/year, ', ',
            $book/cover, ', ',
            $book/price, ', '
        )