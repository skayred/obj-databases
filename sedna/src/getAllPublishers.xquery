declare function local:getPublishersAndYears () as item()* {
    for $book in doc("books")/all_books/book
        return concat($book/year, ", " , $book/publisher)
};

distinct-values(local:getPublishersAndYears())