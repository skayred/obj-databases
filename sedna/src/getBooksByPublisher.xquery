let $publisher := "Вильямс"

for $book in doc("books")/all_books/book
    where $book/publisher = $publisher
    return concat(
            $book/@id, ', ',
            $book/author[position() = 1], ', ',
            $book/title, ', ',
            $book/city, ', ',
            $book/publisher, ', ',
            $book/year, ', ',
            $book/cover, ', ',
            $book/price, ', '
        )