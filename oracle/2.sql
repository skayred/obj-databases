/* Plain addings */
/* 1 */
INSERT INTO stocks VALUES(1004, 6750.00, 2);
INSERT INTO stocks VALUES(1011, 4500.23, 2);
INSERT INTO stocks VALUES(1534, 2234.00, 2);
INSERT INTO stocks VALUES(1535, 3456.23, 2);
INSERT INTO stocks VALUES(1734, 7843.28, 3);

/* 2 */
INSERT INTO customers VALUES (
    1
    , 'Bruce Wayne'
    , Address('2 Avocet Drive', 'Redwood Shores', 'CA', '95054')
    , PhoneType('415-555-0102')
);
	
INSERT INTO customers VALUES (
    2
    , 'Tony Stark'
    , Address('323 College Drive', 'Edison', 'NJ', '08820')
    , PhoneType('609-555-0190','201-555-0140')
);

INSERT INTO customers VALUES (
    3
    , 'Peter Parker'
    , Address('177 Sunset Blvd', 'LA', 'CA', '90210')
    , PhoneType('NULL')
);

INSERT INTO customers VALUES (
    4
    , 'Clark Kent'
    , Address('85 Infinite Loop', 'Cupertiono', 'CA', '95014')
    , PhoneType('123-456-7890','987-654-3210')
);	

/* 3 */
INSERT INTO PurchaseOrders
  SELECT  
    1
    , SYSDATE
    , SYSDATE + 7
    , REF(C)
    , LineItemList()
    , NULL
  FROM
    customers C
  WHERE  
    C.custNo = 1;

INSERT INTO PurchaseOrders
  SELECT
    2
    , SYSDATE
    , SYSDATE + 7
    , REF(C)
    , LineItemList()
    , NULL
  FROM   
    customers C
  WHERE  
    C.custNo = 2;

INSERT INTO PurchaseOrder_objtab
  SELECT  3, SYSDATE, SYSDATE+7, REF(C),
          LineItemList_ntabtyp(),
          NULL
   FROM   Customer_objtab C
   WHERE  C.CustNo = 3;

INSERT INTO PurchaseOrder_objtab
  SELECT  4, SYSDATE, SYSDATE+7, REF(C),
          LineItemList_ntabtyp(),
          NULL
   FROM   Customer_objtab C
   WHERE  C.CustNo = 4;


/* Embedded addings */
INSERT INTO TABLE (
  SELECT P.LineItemLists
    FROM   
      PurchaseOrders P
    WHERE  
      P.pONo = 1
  )
  SELECT  
    01
    , 12
    , 0
    , REF(S)
  FROM   
    Stocks S
  WHERE  
    S.stockNo = 1534;

/* Selections */
/* 1 */
SELECT 
  custName 
FROM 
  customers 
ORDER BY 
  custName;

/* 2 */
SELECT
  p.poNo
  , p.sumLineItems()
FROM
  purchaseOrders p;

/* 3 */
SELECT
  po.poNo
  , po.orderDate
  , po.shipDate
  , po.cust_ref.custName      
FROM
  purchaseOrders po;

/* 4 */
SELECT
  po.PONo
  , po.cust_ref.custNo
  , L.*
FROM
  purchaseOrders po
  , TABLE (po.lineItemLists) L
WHERE
  L.Stock_ref.stockNo = 1004;

/* 5 */
SELECT 
  MAX(L.DISCOUNT)
FROM 
  purchaseOrders po
  , TABLE (po.lineItemLists) L;
