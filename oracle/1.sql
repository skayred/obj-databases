CREATE OR REPLACE TYPE Address AS OBJECT(
  street VARCHAR2(100)
  , city VARCHAR2(100)
  , state CHAR(2)
  , zip VARCHAR2(10)
);

CREATE OR REPLACE TYPE Customer AS OBJECT (
  custNo NUMBER
  , custName VARCHAR(100)
  , address Address
  , phone PhoneType

  , ORDER MEMBER FUNCTION
    compareCustOrders (x IN Customer) RETURN INTEGER
);

CREATE OR REPLACE TYPE LineItem AS OBJECT(
  LineItemNo NUMBER
  , Stock_ref REF StockItem
);

CREATE OR REPLACE TYPE LineItemLists AS TABLE OF LineItem;

CREATE OR REPLACE TYPE PhoneType AS VARRAY(10) OF VARCHAR2(20);

CREATE OR REPLACE TYPE PurchaseOrder AS OBJECT (
  poNo number
  , orderDate DATE
  , shipDate DATE
  , cust_ref REF Customer
  , lineItemList_ntab LineItemList_ntabtyp
  , shipToAddr_obj Address_objtyp
  , map MEMBER function getPONo RETURN NUMBER
  , MEMBER function sumLineItems RETURN NUMBER
);

CREATE OR REPLACE TYPE StockItem AS OBJECT (
  stockNo number
  , price number
  , taRate number
);

CREATE OR REPLACE TYPE BODY Customer AS
  ORDER MEMBER FUNCTION compareCustOrders(x IN Customer_objtyp) RETURN INTEGER IS
  BEGIN
    RETURN self.custNo - x.custNo;
  END compareCustOrders;
END;

CREATE OR REPLACE TYPE BODY PurchaseOrder AS
MAP MEMBER FUNCTION getPONo RETURN NUMBER IS
BEGIN
  RETURN poNo;
END getPoNo;

MEMBER FUNCTION sumLineItems RETURN NUMBER IS
  BEGIN
    return sumLineItems;
  END sumLineItems;
END;

CREATE TABLE 
  purchaseOrders 
OF 
  PurchaseOrder (  
    PRIMARY KEY (poNo)
    , FOREIGN KEY (cust_ref) REFERENCES customers)        
    OBJECT ID PRIMARY KEY                                     
    NESTED TABLE LineItemLists STORE AS poLines (     
      (PRIMARY KEY(NESTED_TABLE_ID, lineItemNo))              
      ORGANIZATION INDEX COMPRESS)                            
    RETURN AS LOCATOR
   
CREATE TABLE
  customers
OF 
  Customer (custNo PRIMARY KEY)
OBJECT IDENTIFIER IS PRIMARY KEY

CREATE TABLE 
  stocks
OF 
  StockItem (StockNo PRIMARY KEY)
OBJECT ID PRIMARY KEY

ALTER TABLE poLines
   ADD (SCOPE FOR (Stock_ref) IS stocks);
