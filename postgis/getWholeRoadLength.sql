SELECT 
  a.name
  , SUM(ST_Length(ST_Transform(r.the_geom, 32630))) /1000 AS wholeLength 
FROM 
  admin a
  , "road-l-osm" r 
WHERE 
  ST_Contains(a.the_geom, r.the_geom) 
  AND a.name = 'Новосибирская область' 
GROUP BY 
  a.name;
