SELECT 
  a1.name
  , a2.name 
FROM 
  admin a1 
  LEFT JOIN admin a2 
    ON ST_Intersects(a1.the_geom, a2.the_geom) 
      AND a1.id <> a2.id 
WHERE 
  a1.id < a2.id;
