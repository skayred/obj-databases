SELECT 
  a.name
  , o.name_r
  , ST_Area(ST_Transform(o.the_geom, 32630)) 
FROM 
  oopt o 
  LEFT JOIN admin a 
    ON ST_Contains(a.the_geom, o.the_geom);
