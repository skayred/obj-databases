SELECT 
  ST_Area(ST_Transform(s1.the_geom, 32630)) / 1000000 AS area
FROM 
  settlements s1 
WHERE 
  name = 'Новосибирск';
