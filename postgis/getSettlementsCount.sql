SELECT 
  a.name
  , COUNT(s) 
FROM 
  admin a
  , settlements s 
WHERE 
  ST_Contains(a.the_geom, s.the_geom) 
GROUP BY 
  a.name;
