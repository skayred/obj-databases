SELECT 
  s1.name
  , s2.name
  , ST_Distance(ST_Transform(s1.the_geom, 32630), ST_Transform(s2.the_geom, 32630)) / 1000
FROM 
  settlements s1
  , settlements s2 
WHERE 
  s1.name = 'Кемерово' 
  AND s2.name = 'Новосибирск';

