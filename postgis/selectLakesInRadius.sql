SELECT 
  COUNT(h) 
FROM 
  "hydro-a" h
  , settlements s 
WHERE 
  h.name ~* 'озеро' 
  AND s.name = 'Барнаул' 
  AND ST_Distance(ST_Transform(h.the_geom, 32630), ST_Transform(s.the_geom, 32630)) < 100000;

