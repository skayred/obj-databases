START 
  n=node(*)
MATCH
  (n)<-[:IS_FRIEND]-(friend)
RETURN
  n.name, count(*)
ORDER BY
  n.name
