START 
  n=node(*) 
WHERE
  n.name = 'Ivan G'
MATCH
  (n)<-[:IS_FRIEND*2]-(friend_of_friend)
RETURN DISTINCT
  friend_of_friend.name
ORDER BY
  friend_of_friend.name
