START 
  n=node(*) 
WHERE
  n.name = 'Ivan G'
MATCH
  (n)<-[:IS_FRIEND]-(friend)
RETURN 
  friend.name
ORDER BY
  friend.name
