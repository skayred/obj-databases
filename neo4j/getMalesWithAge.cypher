START 
  n=node(*) 
WHERE
  n.sex = 'male'
RETURN 
  n.name, n.age 
ORDER BY 
  n.age DESC
