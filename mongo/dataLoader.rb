#!/usr/bin/ruby

require 'csv'
require 'json'

logParsed = []
CSV.foreach(ARGV[0]) do |row|
  logParsed << {
    :url => row[0],
    :ip => row[1],
    :timestamp => row[2].to_f,
    :time => row[3].to_f
  }
end

resultFile = File.new(ARGV[1], "w")
resultFile.write(logParsed.to_json);
resultFile.close
