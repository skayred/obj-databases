conn = new Mongo();
db = conn.getDB('lab');

var startTimestamp = 0;
var endTimestamp = 1383774334;

var logs = db.logs.find(
{
  timestamp : {
    $gte : startTimestamp,
    $lte : endTimestamp
  }
}, { 
  url: 1 
}).sort({ url: 1 });
while (logs.hasNext()) {
    var log = logs.next();
    print(log.url);
}
