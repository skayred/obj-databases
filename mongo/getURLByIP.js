conn = new Mongo();
db = conn.getDB('lab');

var ip = "192.168.0.1";

var logs = db.logs.find({ ip: ip}, { url: 1 }).sort({ url: 1 });
while (logs.hasNext()) {
    var log = logs.next();
    print(log.url);
}
