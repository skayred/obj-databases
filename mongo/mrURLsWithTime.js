conn = new Mongo();
db = conn.getDB("lab");

db.logs.mapReduce(
  function() { emit(this.url, this.time); }, // map
  function(key, values) { return Array.sum(values) }, // reduce
  {
    query: {},
    out: "result"
  }
);

var result = db.result.find().sort({ _id: -1 });

while (result.hasNext()) {
  var item = result.next();
  print(item._id + " -> " + item.value + " seconds");
}
