conn = new Mongo();
db = conn.getDB("lab");

db.logs.mapReduce(
  function() { 
    emit(this.ip, { count: 1, time: this.time}); 
  }, // map
  function(key, values) { 
    var wholeTime = 0;
    for (var i = 0 ; i < values.length ; i++) { wholeTime += values[i].time; }

    return { count: values.length, time: wholeTime }; }, // reduce
  {
    query: { },
    out: "result"
  }
);

var result = db.result.find().sort({ _id: 1, value: 1 });

while (result.hasNext()) {
  var item = result.next();
  print(item._id + " -> " + item.value.count + " times and " + item.value.time + " seconds");
}
