conn = new Mongo();
db = conn.getDB("lab");

var startTimestamp = 0;
var endTimestamp = 1386642573;

db.logs.mapReduce(
  function() { 
    var date = new Date(this.timestamp * 1000);
    emit({url: this.url, date: (date.getDay() + 1) + "." + (date.getMonth() + 1) + "." + date.getFullYear() }, 1); 
  }, // map
  function(key, values) { return values.length; }, // reduce
  {
    query: { timestamp: {$gte : startTimestamp, $lte : endTimestamp} },
    out: "result"
  }
);

var result = db.result.find().sort({ value: -1 });

while (result.hasNext()) {
  var item = result.next();
  print(item._id.url + ", " + item._id.date + " -> " + item.value + " requests");
}
