conn = new Mongo();
db = conn.getDB('lab');

var logs = db.logs.find({}, { url: 1 }).sort({ url: 1 });
while (logs.hasNext()) {
    var log = logs.next();
    print(log.url);
}
